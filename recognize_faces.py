# import the necessary packages
import face_recognition
import pickle
import argparse
import cv2
from imutils.video import VideoStream
import imutils
import time
import os

import smtplib

from string import Template

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

MY_ADDRESS = 'engitupsilon2019@gmail.com'
PASSWORD = 'engitcs2019'
#Screen resolution 
screenW = 1080
screenH = 1920

"""
This file assumes the MacBackup drive is connected.

--encodings-file encodings/jpark.pkl --output output/webcam_output.avi --display 1

To test using laptop video camera:
--encodings-file encodings/jpark.pkl --input camera

ap.add_argument("-i", "--input", type=str, required=False,
                default='/Volumes/MacBackup/PyImageSearch/face-recognition-opencv/videos/lunch_scene.mp4',
                help="path to input video or the word 'camera' to capture video from webcam")

"""


def get_contacts(filename):
    """
    Return two lists names, emails containing names and email addresses
    read from a file specified by filename.
    """

    names = []
    emails = []
    with open(filename, mode='r', encoding='utf-8') as contacts_file:
        for a_contact in contacts_file:
            names.append(a_contact.split()[0])
            emails.append(a_contact.split()[1])
    return names, emails


def read_template(filename):
    """
    Returns a Template object comprising the contents of the
    file specified by filename.
    """

    with open(filename, 'r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)

def send_mail(img, visitor_name):
    print("get_contacts")
    names, emails = get_contacts('mycontacts.txt') # read contacts
    print("read_template")
    message_template = read_template('message.txt')

    # set up the SMTP server
    s = smtplib.SMTP(host='smtp.gmail.com', port=587)
    print("start")
    s.starttls()
    print("starttls")
    s.login(MY_ADDRESS, PASSWORD)
    print("login")
    # For each contact, send the email:
    for name, email in zip(names, emails):
        msg = MIMEMultipart()       # create a message

        # add in the actual person name to the message template
        message = message_template.substitute(PERSON_NAME=name.title(), VISITOR_NAME=visitor_name)

        # Prints out the message body for our sake
        print(message)

        # setup the parameters of the message
        msg['From']=MY_ADDRESS
        msg['To']=email
        msg['Subject']="The  visitor is coming"

        # add in the message body
        msg.attach(MIMEText(message, 'plain'))

        # write the HTML part
        html = """\
        <html>
            <body>
                    <img src="cid:Mailtrapimage">
            </body>
        </html>
        """

        part = MIMEText(html, "html")
        msg.attach(part)

        # We assume that the image file is in the same directory that you run your Python script from
        fp = open(img, 'rb')
        image = MIMEImage(fp.read())
        fp.close()

        # Specify the  ID according to the img src in the HTML part
        image.add_header('Content-ID', '<Mailtrapimage>')
        msg.attach(image)

        # send the message via the server set up earlier.
        s.sendmail(MY_ADDRESS, email, msg.as_string())
        #s.send_message(msg)
        print('sent')
        del msg
    # Terminate the SMTP session and close the connection
    s.quit()

ap = argparse.ArgumentParser()
ap.add_argument("-e", "--encodings-file", required=False, default='encodings/friends_family_encodings.pkl',
                help="path to serialized db of facial encodings")
ap.add_argument("-m", "--detection-method", type=str, default='hog',
                help="face detection model to use: either 'hog' or 'cnn' ")
ap.add_argument("-o", "--output", type=str, required=False, help="path to output video DO NOT ADD EXTENSION.  E.g. output/my_test")
ap.add_argument("-y", "--display", type=int, default=1, help="whether or not to display output frame to screen")
ap.add_argument("-i", "--input", type=str, required=False,
                default='camera',
                help="path to input video or the word 'camera' to capture video from webcam")

args = vars(ap.parse_args())

# load the known faces and embeddings
print("[INFO] loading encodings...")
data = pickle.loads(open(args['encodings_file'], "rb").read())

# initialize the video stream and pointer to output video file, then allow the camera
# sensor to warm up
print(f"Input: {args['input']}")
video_file = False
if args['input'] == 'camera':
    print("[INFO] starting video stream...")
    vs = VideoStream(src=0).start()
else:
    print("[INFO] using video file...")
    video_file = True
    vs = cv2.VideoCapture(args['input'])

writer = None
time.sleep(0.2)

# loop over frames from the vdeo file stream
while True:
    # grab the frame from the threaded video stream
    if video_file:
        (grabbed, frame) = vs.read()
        if not grabbed:
            break
    else:
        
        frame = vs.read()
   
    # convert the input frame from BGR to RGB then resize it to have a width
    # of 750px (to speedup processing)
    rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    rgb_image = imutils.resize(rgb_image, width=750)
    #rgb_image = cv2.resize(rgb_image, (screenW, screenH))
    r = frame.shape[1] / float(rgb_image.shape[1])

    # detect the (x,y)-coordinates of the bounding boxes corresponding to each face in the
    # input frame, then compute the facial embeddings for each face
    boxes = face_recognition.face_locations(rgb_image, model=args["detection_method"])
    encodings = face_recognition.face_encodings(rgb_image, boxes)

    names = []

    # loop over the facial embeddings
    for encoding in encodings:
        # attempt to match each face in the input image to our known encodings
        matches = face_recognition.compare_faces(data['encodings'], encoding, tolerance=0.5)
        name = "Unknown"

        # check to see if we have found any matches
        if True in matches:
            # find the indexes of all matched faces then initialize a dictionary to count
            # the total number of times each face was matched
            matchedIdxs = [i for (i, b) in enumerate(matches) if b]
            counts = {}

            # loop over the matched indexes and maintain a count for each recognized face face
            for i in matchedIdxs:
                name = data['names'][i]
                counts[name] = counts.get(name, 0) + 1

            # determine the recognized face with the largest number of votes: (notes: in the event of an unlikely
            # tie, Python will select first entry in the dictionary)
            name = max(counts, key=counts.get)
        names.append(name)

    # loop over the recognized faces
    for ((top, right, bottom, left), name) in zip(boxes, names):
        # rescale the face coordinates
        top = int(top * r)
        right = int(right * r)
        bottom = int(bottom * r)
        left = int(left * r)
        count = 0
        # draw the predicted face name on the image
        cv2.rectangle(frame, (left, top), (right, bottom),
                      (0, 255, 0), 2)
        y = top - 15 if top - 15 > 15 else top + 15
        if(name != "Unknown"):
            cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
                    0.75, (0, 255, 0), 2)
            cv2.imwrite("original_images/visitor.jpg", frame)
            send_mail("original_images/visitor.jpg", name)
            os.remove("original_images/visitor.jpg")
        else:
            cv2.imwrite("unknown_person/unknow_" + str(count) + ".jpg", frame)
            count = count + 1
            cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
                    0.75, (0, 255, 0), 2)
    # if the video writer is None *AND* we are supposed to write
    #g the output video to disk initialize the writer
    if writer is None and args["output"] is not None:
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        writer = cv2.VideoWriter(args["output"], fourcc, 20,
                                 (frame.shape[1], frame.shape[0]), True)

    # if the writer is not None, write the frame with recognized
    # faces to disk
    if writer is not None:
        writer.write(frame)

    # check to see if we are supposed to display the output frame to
    # the screen
    if args["display"] > 0:
        frame = cv2.resize(frame, (screenH, screenW))
        cv2.imshow("Epsilon Face's Rec", frame)
        key = cv2.waitKey(1) & 0xFF

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

# close the video file pointers
try:
    if video_file:
        vs.release()
except:
    pass

# check to see if the video writer point needs to be released
if writer is not None:
    writer.release()

if args['output']:
    # for some reason, we cannot write the file with the .mp4 extension so we write it without the extension
    # but to get quicktime to play it, the file has to have the .mp4 extension.
    os.rename(args['output'], f"{args['output']}.mp4")
